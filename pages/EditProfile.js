import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, Input, List, SearchBar, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'

class ProfilePage extends Component {

    state = {
        firstName: '',
        lastName: '',
    }

    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    goToProfilePage = () => {
        this.props.history.push('/ProfilePage')
    }

    goToDataPage = () => {
        this.props.history.push('/DataPage')
    }

    goToAlertPage = () => {
        this.props.history.push('/AlertPage')
    }

    render() {
        return (
            <ImageBackground source={require('./119.jpeg')} style={{height: 200 }}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.avatar} source={require('./119.jpeg')} />
                </View>
            

                <Image style={styles.avatar} source={require('./120.jpg')} />
                <View style={styles.body}>

                    <View style={styles.bodyContent}>
                        {/* username */}
                        <Text style={styles.name}> Min Mintra </Text>  
                        {/* email */}
                        <Text style={styles.info}> mintra_panyana@cmu.ac.th </Text>

                        <Card >
                        
                        </Card>

                            

                    </View>
                </View>
            </View>
            </ImageBackground>
        );
    }
}
export default ProfilePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },
    body: {
        marginTop: 40,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },
    name: {
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        fontSize: 14,
        color: "#00BFFF",
        marginTop: 10
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    buttonContainer: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
});
