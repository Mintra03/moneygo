import React, { Component } from 'react';
import { StyleSheet, Image, View, ImageBackground, Text, ScrollView } from 'react-native';
import { Button, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';

class LoginPage extends Component {

    state = {
        email: 'test2@gmail.com',
        password: '123456',
        username: '',
        firstName: '',
        lastName: '',
        token: '',
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    onPressLogIn = () => {
        axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
            email: this.state.username, password: this.state.password
        }).then((response) => {
            console.log('Login success: ', response);
            this.setState({
                token: response.data.user.token, firstname: response.data.user.firstName,
                lastname: response.data.user.lastName
            })
            this.props.onSuccess(this.state)
            this.props.goSummary()
            
        }).catch(function (error) {
            console.log('error: ', error.response.data.errors)
        });

       
    }

    componentDidMount=()=>{
        console.log('Props: ', this.props)
    }

    gotoRegister = () => {
        this.props.history.push('/RegisterPage', {})
    }
    // onClickLoginPage = () => {
    //     const { addTodo, addUser } = this.props
    //     this.props.history.push('/MainPage', addTodo(this.state.email), addUser())
    // }

    // changeText = (field, text) => {
    //     this.setState({ [field]: text })
    // }

    // onClickRegister = () => {
    //     const { addTodo, addUser } = this.props
    //     this.props.history.push('/RegisterPage', addTodo(this.state.email), addUser())
    // }

    render() {
        return (
            <ImageBackground source={require('./12.jpg')} style={{ width: '100%', height: '100%' }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={[styles.control, styles.center]}>
                        
                        <View style={styles.container} >
                            <View style={styles.content}>
                                <View style={[styles.box1, styles.center]}>
                                <Text style={[styles.headertext, styles.center]}>MONEY...GO </Text>
                                    <Image source={require('./21.png')} style={[styles.logo, styles.center]} />
                                </View>

                                <View style={[styles.box2, styles.center]}>

                                    <Text style='paddingLeft=15' > Email Address </Text>
                                    <View style={[styles.textInput1, styles.center]}>
                                        <InputItem
                                            value={this.state.email}
                                            onChangeText={value => {
                                                this.changeValue(
                                                    'email', value
                                                );
                                            }}
                                            clear
                                            placeholder='Email'
                                        />
                                    </View>

                                    <Text> Password </Text>
                                    <View style={[styles.textInput1, styles.center]}>
                                        <InputItem
                                            value={this.state.password}
                                            onChangeText={value => {
                                                this.changeValue(
                                                    'password', value,
                                                );
                                            }}
                                            clear
                                            placeholder='Password'
                                        />
                                    </View>

                                    <View style={[styles.button, styles.center]}>
                                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                                        style={styles.backgroundColor} onPress={this.onPressLogIn}>Login</Button>
                                    </View>
                                    <Text> or </Text>
                                    <View style={[styles.button, styles.center]}>
                                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                                        style={styles.backgroundColor} onPress={this.gotoRegister}>Register</Button>
                                    </View>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         user: state.user
//     }
// }
// const mapDispatchToProps = (dispatch) => {
//     return {
//         addTodo: (email) => {
//             dispatch({
//                 type: 'USER_LOGIN',
//                 email: email,
//                 password: '',

//             })
//         },

//         addUser: () => {
//             dispatch({
//                 type: 'ADD_USER',
//                 firstname: '',
//                 lastname: '',
//                 image: '',

//             })
//         }
//     }
// }


export default LoginPage


const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },

    container: {
        flex: 1
    },
    content: {
        flex: 1,
        flexDirection: 'column'
    },

    headertext: {
        color: '#8E26B7',
        fontSize: 36,
        fontWeight: 'bold',
        padding: 0.5,
    },

    box1: {
        flex: 1,
        flexDirection: 'column'
    },

    box2: {
        flex: 1,
        flexDirection: 'column',
        margin: 6,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 150,
        width: 280,
        height: 280
    },

    textInput1: {
        backgroundColor: '#EAC8F3',
        flex: 1,
        padding: 10,
        margin: 6,
    },

    button: {
        flex: 1,
        margin: 6,
    },
    margin: {
        margin: 10,
    },

    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
    },

    buttonBox: {
        color: 'pink',
    },

    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },

    control: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        width: 360,
        height: 790,
        paddingLeft: 60,
        paddingRight: 60,
        margin: 10,
        marginTop: 30,
        marginLeft: 26,
        padding: 12,
    },

});
