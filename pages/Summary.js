import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, Button } from 'react-native';


class SummaryPage extends Component {
    state = {
        wallet: []
    }

    componentDidMount = () => {
        console.log('Susunaja', this.props.wallet)
        this.setState({ wallet: this.props.wallet })
    }
    goToEditWallet = (item, index) => {
        const { goToEditWallet } = this.props
        let sentItem = {
            index: index,
            walletName: item.walletName,
            money: item.money
        }
        console.log('edit: ', sentItem)
        this.setState({ wallet: []})
        goToEditWallet(sentItem)
       
    }
    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.goToEditWallet(item, index)}>
                <Text > wallet: {item.walletName}  money:  {item.money} </Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text> Summary </Text>
                <TouchableOpacity onPress={this.props.goToAddWallet}>
                    <Text style={{ marginLeft: '40%' }}> Add </Text>
                </TouchableOpacity>
                <View style={{ marginLeft: '25%' }}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.wallet}
                        renderItem={this.renderItem}
                    />

                </View>
            </View>
        )
    }
}
export default SummaryPage
