import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView, TextInput } from 'react-native';
import { Button, Icon, Flex, Card, SearchBar } from '@ant-design/react-native';
import { connect } from 'react-redux'

class showListPage extends Component {

    state = {
        today: new Date(),
        index: null,
        walletName: '',
        money: 0,
        newAmountOfMoney: '',
        topic: '',
        type: '',
        transaction: []
    }

    componentDidMount() {
        this.getDefaultValue()


    }
    getDefaultValue = () => {
        const { item } = this.props
        this.setState({ ...item })

    }

    changeValue = (state, value) => this.setState({ [state]: value })

    onAddLedger = () => {
        let newValue = parseInt(this.state.money, 10)

        const amountOfMoney = parseInt(this.state.newAmountOfMoney, 10)
        if (this.state.type === 'i') {
            newValue = newValue + amountOfMoney
        } else {
            newValue = newValue - amountOfMoney
        }
        console.log('you have: ', newValue)
        let newItem = {
            walletName: this.state.walletName,
            money: newValue
        }
        this.setState({ money: newValue })
        let transaction = {
            date: this.state.today,
            transaction: this.state.transaction
        }

        this.createTransaction(newValue)

        this.props.onSave(this.state.index, newItem, transaction)
        // this.props.goBack()
    }
    createTransaction = (newValue) => {
        console.log('newValue', newValue)
        let newTracnsaction = {
            topic: this.state.topic,
            type: this.state.type,
            newAmountOfMoney: this.state.newAmountOfMoney,
            date: new Date().toDateString(),
            walletName: this.state.walletName,
            money: newValue

        }
        this.setState({
            transaction: [...this.state.transaction, newTracnsaction]
        })

    }
    goBack = () => {
        const { goBack } = this.props
        let transaction = {
            transaction: this.state.transaction
        }

        this.props.addTransaction(transaction)
        goBack()
    }


    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    goToEdit = () => {
        this.props.history.push('/EditPage')
    }

    goToAddinfo = () => {
        this.props.history.push('/AddinfoPage')
    }

    goToSelectPage = () => {
        this.props.history.push('/SelectPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={[styles.boxIcon, styles.center]}>
                        <Icon
                            style={styles.iconheaderleft}
                            name='arrow-left'
                            size={30}
                            color='#D365EF'
                            onPress={this.goBack}
                        // onPress={this.goToMainPage}
                        />
                    </View>

                    <View style={[styles.boxheader, styles.center]}>
                        <Image source={require('./16.png')} style={[styles.image, styles.logo1]} />
                        <TextInput
                            placeholderTextColor='#cccccc'
                            clear
                            placeholder='Wallet name'
                            value={this.state.walletName}
                            style={[styles.headertext1]}
                            onChangeText={value => this.changeValue
                                ('walletName', value)} >
                        </TextInput>
                    </View>

                    <View style={[styles.boxIcon, styles.center]}>
                        <TouchableOpacity onPress={this.goToEdit}>
                            <View style={[styles.boxIcon, styles.center]}>
                                <Text style={styles.textHeader}> Edit </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <ScrollView contentContainerStyle={styles.contentContainer}>
                        <View style={[styles.box1, styles.center]}>
                            <View style={[styles.textInput1, styles.center]}>
                                <Text style={styles.headertext}> เงินในกระเป๋า </Text>
                                <TextInput 
                                placeholderTextColor='#cccccc' 
                                style={styles.text}
                                clear placeholder='0.00' 
                                value={this.state.money}
                                onChangeText={value => this.changeValue('money', value)} >
                                </TextInput>

                                {/* <Text style={styles.headertext}><Text style={styles.text}>0</Text>.00</Text> */}
                                <Text style={styles.headertext2}> _____________________________ </Text>
                                {/* <Text style={styles.text1}> เดือนนี้เหลือเงิน : 0.00 </Text> */}
                                <TextInput 
                                placeholderTextColor='#cccccc' 
                                style={styles.text1}
                                clear placeholder='เดือนนี้เหลือ : 0.00' 
                                value={this.state.money}
                                onChangeText={value => this.changeValue('money', value)} >
                                </TextInput>
                            </View>
                        </View>
                    </ScrollView>

                    <View style={[styles.box2, styles.center]}>
                        <View style={[styles.inbox1]}>
                            <Image source={require('./42.png')} style={[styles.logo]} />
                            <View style={[styles.inbox3]}>
                                <Text style={[styles.headertext3]}> ก.พ. 2019 </Text>
                                <Text style={styles.text3}> รายรับ : 0.00 </Text>
                                <Text style={styles.text3}> รายจ่าย : 0.00 </Text>
                            </View>
                        </View>

                        <View style={[styles.inbox2]}>
                            <Image source={require('./46.png')} style={[styles.logo2]} />
                            <Text style={styles.text2}> รายการใช้จ่าย </Text>
                        </View>
                    </View>

                    <ScrollView>
                        {this.state.transaction !== [] ? (
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                data={this.state.transaction}
                                extraData={this.state}
                                renderItem={this.renderItem}
                            />
                        ) : (
                                <View></View>
                            )}
                    </ScrollView>

                    <TouchableOpacity onPress={this.goToSelectPage} >
                        <Image source={require('./52.png')} style={[styles.logo3, styles.center]} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default showListPage

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#4A1358',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.1,
    },
    headertext: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 2,
    },
    headertext1: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 2,
    },
    headertext2: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        top: -10
    },
    headertext3: {
        color: '#9E2FBF',
        fontSize: 21,
        fontWeight: 'bold',
        marginLeft: 16,
    },
    textHeader: {
        color: '#D365EF',
        fontSize: 15.5,
        fontWeight: 'bold',
        padding: 6,
        marginBottom: 2,
    },
    content: {
        flex: 1,
        backgroundColor: '#E2DFE3',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        flex: 0.3,
        flexDirection: 'row'
    },
    boxheader: {
        backgroundColor: '#4A1358',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 400,
        height: 69,
        flexDirection: 'row',
    },
    inbox2: {
        backgroundColor: 'white',
        width: 400,
        height: 50,
        marginTop: 0.1,
        flexDirection: 'row',
    },
    inbox3: {
        backgroundColor: 'white',
        margin: 2,
        width: 160,
        height: 30,
        flexDirection: 'column',
    },
    logo: {
        width: 30,
        height: 30,
        marginTop: 18,
        marginLeft: 40,
    },
    logo1: {
        width: 26,
        height: 26,
    },
    logo2: {
        width: 36,
        height: 30,
        marginTop: 10,
        marginLeft: 40,
    },
    logo3: {
        width: 112,
        height: 112,
        top: 36,
    },
    logo4: {
        width: 36,
        height: 30,
        marginTop: 10,
        marginLeft: 40,
    },
    image: {
        backgroundColor: '#F3D6FB',
        width: 24,
        height: 24,
        borderRadius: 15,

    },
    box1: {
        flex: 0.3,
        flexDirection: 'column',
        backgroundColor: '#E2DFE3',
    },
    box2: {
        backgroundColor: '#D1D1D1',
        flex: 0.7,
        borderRadius: 0,
        width: 400,
        top: -159,
    },
    box3: {
        backgroundColor: '#DBD9DB',
        flex: 0.5,
    },
    box4: {
        backgroundColor: 'white',
        flex: 0.1,
    },
    textInput1: {
        width: 260,
        height: 136,
        borderRadius: 12,
        fontSize: 18,
        backgroundColor: '#4B2256',
        color: 'white',
        marginHorizontal: 3,
        marginBottom: 3,
        marginTop: 1,
    },
    text: {
        marginTop: 10,
        color: 'white',
        fontSize: 26,
        fontWeight: 'bold',
        marginBottom: 10
    },
    text1: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'rgba(255,255,255,0.7)',
        marginTop: 1,
    },
    text2: {
        color: '#8529A0',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 6,
        marginTop: 8,
        marginLeft: 15,
    },
    text3: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#49454A',
        marginTop: 1,
        marginLeft: 20,
    },


});