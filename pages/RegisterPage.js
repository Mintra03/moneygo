import React, { Component } from 'react';
import { StyleSheet, Image, View, ImageBackground, Text, ScrollView } from 'react-native';
import { Button, InputItem } from '@ant-design/react-native';
import axios from 'axios';

class RegisterPage extends Component {

    state = {
        firstname: '',
        lastname: '',
        username: '',
        email: '',
        password: '',
        confirmPass: '',
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    onClickRegister = () => {
        axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
            email: this.state.username, password: this.state.password,
            firstName: this.state.firstname, lastName: this.state.lastname
        }).then((response) => {
            console.log('Register success: ', response);
            this.props.backToLogin()
        }).catch(function (error) {
            console.log('error: ', error.response.data.errors)
        });
    }

    onClickLoginPage = () => {
        alert('bbb')
        this.props.history.push('/MainPage')
    }

    // onClickRegister = () => {
    //     const { addTodo, addUser } = this.props
    //     this.props.history.push('/LoginPage', addTodo(this.state.email), addUser())
    // }

    // onClickLoginPage = () => {
    //     const { addTodo, addUser } = this.props
    //     this.props.history.push('/MainPage', addTodo(this.state.email), addUser())
    // }

    render() {
        return (
            <ImageBackground source={require('./12.jpg')} style={{ width: '100%', height: '100%' }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={[styles.control, styles.center]}>

                        <View style={styles.container} >
                            <View style={styles.content}>

                            <Text style={styles.headertext}> Welcom to Register </Text>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.firstname}
                                        onChange={value => {
                                            this.changeValue(
                                                'firstname', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Firstname'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.lastname}
                                        onChange={value => {
                                            this.changeValue(
                                                'lastname', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Lastname'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.username}
                                        onChange={value => {
                                            this.changeValue(
                                                'username', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Username'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.password}
                                        onChange={value => {
                                            this.changeValue(
                                                'password', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Password'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.confirmPass}
                                        onChange={value => {
                                            this.changeValue(
                                                'confirmPass', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Confirm Pass'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.email}
                                        onChange={value => {
                                            this.changeValue(
                                                'email', value,
                                            );
                                        }}
                                        clear
                                        placeholder='Email'
                                    />
                                </View>

                                <View style={[styles.button, styles.center]}>
                                    <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                                    style={styles.backgroundColor} onPress={this.onClickRegister}> Register </Button>
                                </View>

                            </View>
                        </View>
                    </View>
                    <View style={[styles.button, styles.center]}>
                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                        style={styles.backgroundColor} onPress={this.onClickRegister}> {"<"}  Back to Login </Button>
                    </View>
                </ScrollView>
            </ImageBackground>

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         user: state.user
//     }
// }
// const mapDispatchToProps = (dispatch) => {
//     return {
//         addTodo: (email) => {
//             dispatch({
//                 type: 'USER_LOGIN',
//                 email: email,
//                 password: '',

//             })
//         },

//         addUser: () => {
//             dispatch({
//                 type: 'ADD_USER',
//                 firstname: '',
//                 lastname: '',
//                 image: '',

//             })
//         }
//     }
// }


export default RegisterPage


const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },

    container: {
        flex: 1
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        padding: 30,
    },

    headertext: {
        color: '#8E26B7',
        fontSize: 26,
        fontWeight: 'bold',
        padding: 0.5,
        marginBottom: 10,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 150,
        width: 280,
        height: 280
    },

    textInput1: {
        backgroundColor: '#EAC8F3',
        paddingLeft: 20,
        paddingRight: 10,
        padding: 8,
        margin: 8,
    },

    button: {
        // flex: 1,
        margin: 16,
    },
    margin: {
        margin: 10,
    },

    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
    },

    buttonBox: {
        color: 'pink',
    },

    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },

    control: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        width: 360,
        height: 560,
        paddingLeft: 30,
        paddingRight: 30,
        margin: 8,
        marginTop: 30,
        marginLeft: 26,
        padding: 6,
    },

});
