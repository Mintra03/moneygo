import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView, TextInput } from 'react-native';
import { Button, Icon, Flex, Card, SearchBar } from '@ant-design/react-native';
import { connect } from 'react-redux'

class showListPage extends Component {

    state = {
        wallet: '',
    }

    goToRevenuePage = () => {
        this.props.history.push('/RevenuePage')
    }

    goToExpensesPage = () => {
        this.props.history.push('/ExpensesPage')
    }

    render() {
        return (
            <View style={{backgroundColor: 'rgba(255,255,255,0.3)'}}>
                <View style={[styles.content]}>
                        <TouchableOpacity onPress={this.goToRevenuePage} >
                            <View style={[styles.boxIcon]}>
                                <Image source={require('./61.png')} style={[styles.logo]} />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.goToExpensesPage} >
                            <View style={[styles.boxIcon]}>
                                <Image source={require('./62.png')} style={[styles.logo1]} />
                            </View>
                        </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default showListPage

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.9)',
    },
    content: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.9)',
        flexDirection: 'row',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        flex: 0.3,
        flexDirection: 'row',
        width: 150,
        height: 65,
        marginLeft: 45,
        backgroundColor: 'rgba(255,255,255,0.9)',
    },
    logo: {
        width: 68,
        height: 68,
        marginTop: 490,

    },
    logo1: {
        width: 68,
        height: 68,
        marginTop: 490,
    },
    image: {
        backgroundColor: '#F3D6FB',
        width: 24,
        height: 24,
        borderRadius: 15,
    },
    text: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 60,
    },

});