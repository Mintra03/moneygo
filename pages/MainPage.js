import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView } from 'react-native';
import { Button, Icon, Flex, Card, TabBar, InputItem, Grid, List } from '@ant-design/react-native';
import { connect } from 'react-redux'

const Item = List.Item;

class MainPage extends Component {

    state = {
        walletName: '',
        money: ''
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    addWallet = () => {
        const amountOfMoney = parseInt(this.state.money, 10)
        let wallet = {
            walletName: this.state.walletName,
            money: amountOfMoney
        }
        this.props.addTheWallet(wallet)
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'blueTab',
        };
    }

    goToMainPage = () => {
        Alert.alert('gooo!!')
        this.props.history.push('/MainPage')
    }

    goToProfilePage = () => {
        Alert.alert('gooo!!')
        this.props.history.push('/ProfilePage')
    }

    goTocreateWallet = () => {
        this.props.history.push('/createWallet')
    }



    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={[styles.header, styles.center]}>
                        <Image source={require('./24.png')} style={[styles.logo, styles.center]} />
                        <Text style={styles.headertext}> Wallet </Text>
                    </View>

                    <View style={[styles.box1, styles.center]}>
                        <Image source={require('./28.gif')} style={[styles.logo1, styles.center]} />
                        <View style={[styles.textInput1, styles.center]}>
                            <InputItem
                                clear
                                value={this.state.money}
                                onChange={value => {
                                    this.changeValue('walletName', value)
                                }}
                                clear
                                placeholder="0.00"
                            />
                        </View>
                    </View>

                    <Button style={[styles.box2, styles.center]} activeStyle={{ backgroundColor: 'white' }} onPress={this.goTocreateWallet}>
                        <Image source={require('./39.png')} style={[styles.logo2]} />
                        Add Wallet </Button>

                    <View style={[styles.box3, styles.center]}>
                        <ScrollView >
                            <Card >
                                {this.state.transaction !== [] ? (
                                    <FlatList
                                        keyExtractor={(item, index) => index.toString()}
                                        data={this.state.wallet}
                                        renderItem={this.renderItem}
                                    />
                                ) : (
                                        <View style={[styles.box1, styles.center]}>
                                            <Image source={require('./93.gif')} style={[styles.logo1, styles.center]} />
                                            <View style={[styles.textInput1, styles.center]}>
                                                <InputItem
                                                    clear
                                                    value={this.state.walletName}
                                                    // value={this.state.money}
                                                    onChange={value => {
                                                        this.changeValue('walletName', value)
                                                    }}
                                                    clear
                                                    placeholder="0.00"
                                                />
                                            </View>
                                        </View>
                                    )}

                                {/* <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Beauty </Text>
                                        </View>
                                    </Item>

                                    <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Supermarket </Text>
                                        </View>
                                    </Item>

                                    <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Health </Text>
                                        </View>
                                    </Item>

                                    <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Shopping </Text>
                                        </View>
                                    </Item>

                                    <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Travel </Text>
                                        </View>
                                    </Item>

                                    <Item onPress={this.goToRevenuePage}>
                                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                            <Image source={require('./93.png')} style={[styles.logo4]} />
                                            <Text style={styles.text2}> Food </Text>
                                        </View>
                                    </Item> */}
                            </Card>
                        </ScrollView>
                    </View>
                </View>

                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#742688"
                    barTintColor="#f5f5f5"
                >
                    <TabBar.Item
                        title="Home"
                        icon={<Icon name="home" />}
                        selected={this.state.selectedTab === 'blueTab'}
                        onPress={this.goToMainPage}
                    >
                    </TabBar.Item>

                    <TabBar.Item
                        title="Profile"
                        icon={<Icon name="user" />}
                        selected={this.state.selectedTab === 'yellowTab'}
                        onPress={this.goToProfilePage}
                    >
                    </TabBar.Item>
                </TabBar>
            </View>
        );
    }
}

export default MainPage

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#4A1358',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.10,
    },
    headertext: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15,
        marginBottom: 5,
    },
    content: {
        flex: 12,
        backgroundColor: '#662078',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        flex: 0,
        flexDirection: 'row'
    },
    box: {
        margin: 0,
        flex: 0,
        marginTop: 10,
        marginRight: 20,
        padding: 3,
    },
    logo: {
        width: 40,
        height: 40,
    },
    logo1: {
        width: 180,
        height: 150,
        marginTop: 10,
    },
    textInput1: {
        backgroundColor: '#EAC8F3',
        flex: 0.15,
        padding: 37,
        margin: 20,
        borderRadius: 10,
        marginTop: 0,
        marginBottom: 30,
    },
    box1: {
        flex: 0.45,
        flexDirection: 'column',
        padding: 5,
        margin: 5,
    },
    box2: {
        backgroundColor: 'white',
        flex: 0.12,
        borderRadius: 0,
    },
    box3: {
        backgroundColor: '#EAECEE',
        flex: 0.4,
        flexDirection: 'row'
    },
    logo2: {
        width: 55,
        height: 55,
        top: -100,
    },
    logo3: {
        width: 180,
        height: 150,
        marginTop: 10,
    },
    logo4: {
        width: 45,
        height: 45,
        borderRadius: 11,
        marginTop: 2,
        marginLeft: 6,
    },
    text: {
        left: 5,
        fontFamily: 'Waffle Regular',
        color: '#9C35B6',
        fontSize: 30,
        textAlign: 'center'
    },
    text2: {
        color: '#535252',
        fontSize: 14,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginTop: 11,
        marginLeft: 15,
    },
    box4: {
        backgroundColor: 'white',
        flex: 0.1,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 45,
        flexDirection: 'row',
    },

});