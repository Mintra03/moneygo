import { SAVE_CURRENT_USER } from '../constants/appConstants';



export default (state = [], { type, payload }) => {
    switch (type) {
        case SAVE_CURRENT_USER:                       
            return { ...state, ...payload }
        default:
            return state
    }
}
