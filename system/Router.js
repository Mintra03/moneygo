import React, { Component } from 'react'
import { Route, Switch, Redirect, NativeRouter  } from 'react-router-native'
import LoginPage from '../pages/LoginPage'
import RegisterPage from '../pages/RegisterPage'
import MainPage from '../pages/MainPage'
import createWallet from '../pages/createWallet'
import showListPage from '../pages/showListPage'
import EditPage from '../pages/EditPage'
import SelectPage from '../pages/SelectPage'
import TopicIncome from '../pages/TopicIncome'
import TopicExpenses from '../pages/TopicExpenses'
import RevenuePage from '../pages/RevenuePage'
import ExpensesPage from '../pages/ExpensesPage'
import ProfilePage from '../pages/ProfilePage'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                    <Switch>
                        <Route excat path='/LoginPage' component={LoginPage} />
                        <Route excat path='/RegisterPage' component={RegisterPage} />
                        <Route excat path='/MainPage' component={MainPage} />
                        <Route excat path='/createWallet' component={createWallet} />
                        <Route excat path='/showListPage' component={showListPage} />
                        <Route excat path='/EditPage' component={EditPage} />
                        <Route excat path='/SelectPage' component={SelectPage} />
                        <Route excat path='/TopicIncome' component={TopicIncome} />
                        <Route excat path='/TopicExpenses' component={TopicExpenses} />
                        <Route excat path='/RevenuePage' component={RevenuePage} />
                        <Route excat path='/ExpensesPage' component={ExpensesPage} />
                        <Route excat path='/ProfilePage' component={ProfilePage} />
                        {/* <Route excat path='/Editprofile' component={Editprofile} /> */}
                        <Redirect to='/RegisterPage' />
                    </Switch>
                </NativeRouter>
        )
    }
}

export default Router
