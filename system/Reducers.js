import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import userReducer from "../Reducers/userReducer";
import walletReducer from '../Reducers/walletReducer'
import historyReducer from '../Reducers/historyReducer'

const Reducers = history =>
    combineReducers({
        userAccount: userReducer,
        wallet: walletReducer,
        historyOfTransacton: historyReducer,
        router: connectRouter(history)
    })

export default Reducers
