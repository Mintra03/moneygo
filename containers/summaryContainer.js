import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import {ROUTE_TO_ADD_WALLET,ROUTE_TO_EDIT_WALLET } from "../constants/routeConstant.js";
import SummaryPage from "../pages/Summary";

const mapStateToProps = ({ wallet }) => ({
    wallet,
})

const mapDispatchToProps = dispatch => ({
    goToAddWallet: () => {
        dispatch(push(ROUTE_TO_ADD_WALLET))
    },
    goToEditWallet: item =>{
        dispatch(push({pathname: ROUTE_TO_EDIT_WALLET, state: { ...item }}))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SummaryPage)
