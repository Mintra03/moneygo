import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import AddWalletPage from "../pages/addWalletPage";
import {addMoreWallet} from '../actions/appActions'
import {ROUTE_TO_SUMMARY} from '../constants/routeConstant'

const mapStateToProps = ({ wallet }) => ({
    walletName: wallet.walletName,
    money: wallet.money,
})

const mapDispatchToProps = dispatch => ({
    addTheWallet: newWallet => {
        dispatch(addMoreWallet(newWallet))
    },
    goToSummary: () => {
        dispatch(push(ROUTE_TO_SUMMARY))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddWalletPage)
