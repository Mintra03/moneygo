import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import EditWalletPage from "../pages/editWalletPage";
import { editMoreWallet, addTransactionHistory } from "../actions/appActions";

const mapStateToProps = ({ router,historyOfTransacton }) => ({
    item: router.location.state,
    date: historyOfTransacton.date,
    transaction: historyOfTransacton,
})
const mapDispatchToProps = dispatch => ({
    onSave: (index, item, newTransaction) => {
        dispatch(editMoreWallet(index, item))
    },
    addTransaction: newTransaction => {
        dispatch(addTransactionHistory(newTransaction))
    },
    goBack: () => {
        dispatch(goBack())
    },


})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditWalletPage)
